package com.apress.gerber.favrestos.app;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneNumberUtils;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.apress.gerber.favrestos.app.db.RestosDbAdapter;
import com.apress.gerber.favrestos.app.db.RestosSimpleCursorAdapter;


public class RestosActivity extends ActionBarActivity {


    private ListView mListView;
    private RestosDbAdapter mDbAdapter;
    private RestosSimpleCursorAdapter mCursorAdapter;

    private SharedPreferences mPreferences;
    private static final String SORT_ORDER = "sort_order";
    private static final String VERY_FIRST_LOAD = "our_very_first_load";
    private String mSortOrder;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders_layout);

        mListView = (ListView) findViewById(R.id.reminders_list_view);
        mListView.setDivider(null);

        //get the shared preferences
        mPreferences = this.getSharedPreferences(
                "com.apress.gerber.favrestos.app", this.MODE_PRIVATE);


        mDbAdapter = new RestosDbAdapter(this);
        mDbAdapter.open();

        //get the value associated with "very_first_load";  return true if there is no value in SharedPreferences (will happen on the very first time only)
        boolean bFirstLoad = mPreferences.getBoolean(VERY_FIRST_LOAD, true);
        if (bFirstLoad) {

            mDbAdapter.insertSomeRestos();
            //set the flag in preferences so that this block will never be called again.
            mPreferences.edit().putBoolean(VERY_FIRST_LOAD, false).commit();
        }

        mSortOrder = mPreferences.getString(SORT_ORDER, null);
        Cursor cursor = mDbAdapter.fetchAllRestos(getSortOrder());


        //from columns defined in the db
        String[] from = new String[]{
                RestosDbAdapter.COL_NAME,
                RestosDbAdapter.COL_CITY
        };

        //to the ids of views in the layout
        int[] to = new int[]{
                R.id.list_text,
                R.id.list_city
        };

        mCursorAdapter = new RestosSimpleCursorAdapter(
                //context
                RestosActivity.this,
                //the layout of the row - now pulling from our new layout
                R.layout.restos_row,
                //cursor
                cursor,
                //from columns defined in the db
                from,
                //to the ids of views in the layout
                to,
                //flag - not used
                0);


        //the cursorAdapter (controller) is now updating the listView (view) with data from the db (model)
        mListView.setAdapter(mCursorAdapter);

        //when we click an individual item in the listview
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            private Restaurant mRestoClicked;
            private int mIdClicked;

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int masterListPosition, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RestosActivity.this);

                ListView modeList = new ListView(RestosActivity.this);

                String[] stringArray = new String[]{"Edit ",  //0
                        "Share ", //1
                        "Map of ", //2
                        "Dial ",  //3
                        "Yelp site ",  //4
                        "Navigate to ",  //5
                        "Delete ", //6
                        "Cancel " //7
                             };

                ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(RestosActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, stringArray);
                modeList.setAdapter(modeAdapter);
                builder.setView(modeList);
                final Dialog dialog = builder.create();


                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                mIdClicked = getIdFromPosition(masterListPosition);
                mRestoClicked = mDbAdapter.fetchRestoById(mIdClicked);
                dialog.setTitle(mRestoClicked.getName());
                dialog.show();
                modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        FavActionUtility favActionUtility = new FavActionUtility(RestosActivity.this);


                        try {
                            switch (position) {
                                case 0:
                                    //edit
                                    Intent intent = new Intent(RestosActivity.this, EditRestoActivity.class);
                                    intent.putExtra("resto_bundle_key", mRestoClicked);
                                    startActivity(intent);
                                    break;
                                case 1:
                                    //share
                                    String strSubject = "Check out: " + mRestoClicked.getName();
                                    String strMessage = "\n\n"; //give the user some room to type a message
                                    strMessage += "Restaurant: " + mRestoClicked.getName();
                                    strMessage += "\nAddress: " + mRestoClicked.getAddress() + ", " + mRestoClicked.getCity();
                                    strMessage += " \n\nPhone: " + PhoneNumberUtils.formatNumber(mRestoClicked.getPhone());
                                    strMessage += " \nYelp page: " + mRestoClicked.getYelp();
                                    if (mRestoClicked.getFavorite() == 1){
                                        strMessage += "\n[This is one of my favorite restaurants]";
                                    }
                                    strMessage += "\n\nPowered by Favorite Restaurants on Android by Adam Gerber";
                                    favActionUtility.share(strSubject, strMessage );
                                    break;

                                case 2:
                                    //map of
                                    favActionUtility.mapOf(mRestoClicked.getAddress(), mRestoClicked.getCity());
                                    break;
                                case 3:
                                    //dial
                                    favActionUtility.dial(mRestoClicked.getPhone());
                                    break;
                                case 4:
                                    //yelp site
                                    favActionUtility.yelpSite(mRestoClicked.getYelp());
                                    break;
                                case 5:
                                    //navigate to
                                    favActionUtility.navigateTo(mRestoClicked.getAddress(), mRestoClicked.getCity());
                                    break;
                                case 6:
                                    //delete single resto (we need to keep this for devices running 11 or less)
                                    mDbAdapter.deleteRestoById(mIdClicked);
                                    mCursorAdapter.changeCursor(mDbAdapter.fetchAllRestos(getSortOrder()));
                                    break;
                                case 7:
                                    //cancel
                                    //do nothing and then dismiss
                                    break;
                            }
                        } catch (Exception e) {
                            //gracefully handle exceptions
                            favActionUtility.showErrorMessageInDialog(e.getMessage());
                            e.printStackTrace();
                        }

                        dialog.dismiss();
                    }
                });


            }
        });

        //contextual action mode set-up
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.cam_menu, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.menu_item_delete_reminder:
                            for (int nC = mCursorAdapter.getCount() - 1; nC >= 0; nC--) {
                                if (mListView.isItemChecked(nC)) {

                                    mDbAdapter.deleteRestoById(getIdFromPosition(nC));

                                }
                            }
                            mode.finish();
                            mCursorAdapter.changeCursor(mDbAdapter.fetchAllRestos(getSortOrder()));
                            return true;

                    }

                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {

                }
            });

        }
    }

    public String getSortOrder() {
        return mSortOrder;
    }

    public void setSortOrder(String strSortOrder) {
        mSortOrder = strSortOrder;
        mPreferences.edit().putString(SORT_ORDER, strSortOrder).commit();

    }



    private int getIdFromPosition(int nPosition) {
        Cursor cursor = mDbAdapter.fetchAllRestos(getSortOrder());
        cursor.move(nPosition);
        return cursor.getInt(RestosDbAdapter.INDEX_ID);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reminders_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
            case R.id.action_new_overflow:
                //create new Restaurant
                Intent intent = new Intent(RestosActivity.this, EditRestoActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_sort_name:
                setSortOrder(RestosDbAdapter.SORT_NAME_ASC);
                mCursorAdapter.changeCursor(mDbAdapter.fetchAllRestos(getSortOrder()));
                return true;

            case R.id.action_sort_fav:
                setSortOrder(RestosDbAdapter.SORT_FAV_DESC);
                mCursorAdapter.changeCursor(mDbAdapter.fetchAllRestos(getSortOrder()));
                return true;

            case R.id.action_sort_none:
                setSortOrder(RestosDbAdapter.SORT_NONE);
                mCursorAdapter.changeCursor(mDbAdapter.fetchAllRestos(getSortOrder()));
                return true;

            case R.id.action_exit:
                finish();
                return true;

            default:
                return true;


        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDbAdapter.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDbAdapter.open();
        mCursorAdapter.changeCursor(mDbAdapter.fetchAllRestos(getSortOrder()));

    }
}
