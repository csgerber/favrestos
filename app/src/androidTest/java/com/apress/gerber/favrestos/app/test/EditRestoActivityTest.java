package com.apress.gerber.favrestos.app.test;

import android.annotation.TargetApi;
import android.graphics.drawable.ColorDrawable;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.apress.gerber.favrestos.app.EditRestoActivity;
import com.apress.gerber.favrestos.app.R;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by Adam Gerber on 5/22/2014.
 * University of Chicago
 */

//to call this test run > gradlew connectedAndroidTest
//from the command-line
public class EditRestoActivityTest extends ActivityInstrumentationTestCase2<EditRestoActivity> {

    private Button mExtractButton;
    private EditRestoActivity mActivity;
    private EditText mCityField, mNameField;
    private CheckBox mCheckBoxFav;
    private View mViewFav;

    public EditRestoActivityTest(){
            super(EditRestoActivity.class);
        }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();

        //get references to any fields you may need to interact with in your Activity
        mExtractButton = (Button) mActivity.findViewById(R.id.extract_yelp_button);
        mCityField = (EditText) mActivity.findViewById(R.id.restaurant_city);
        mNameField = (EditText) mActivity.findViewById(R.id.restaurant_name);

        mCheckBoxFav = (CheckBox) mActivity.findViewById(R.id.check_favorite);
        mViewFav = mActivity.findViewById(R.id.view_favorite);



    }


    @TargetApi(11)
    public void testCheckFavorite () throws  Throwable {
        runTestOnUiThread(new Runnable() {

            @Override
            public void run() {
                mCheckBoxFav.setChecked(true);
                assertTrue(((ColorDrawable) mViewFav.getBackground()).getColor() == mActivity.getResources().getColor(R.color.orange));
            }
        });
    }




    public void testYelpAsyncTask () throws Throwable {

        final CountDownLatch latch = new CountDownLatch(1);

        mActivity.setYelpTaskCallback(new EditRestoActivity.YelpTaskCallback() {

            @Override
            public void executionDone() {
                latch.countDown();
                assertTrue(mActivity.getYelpResultsData().businesses.size() == 20);


            }
        });

        runTestOnUiThread(new Runnable() {

            @Override
            public void run() {
                 mNameField.setText("McDonalds");
                mCityField.setText("Chicago");
                mExtractButton.performClick();
            }
        });


        latch.await(30, TimeUnit.SECONDS);

    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();

    }


}
